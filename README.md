# Welcome to Wellfound's microblog assessment!
https://drive.google.com/file/d/1APt-A5MRC4swWKfrvhuAAb7ALMpNUtHZ/view?usp=sharing

<BR><BR>

## Contributions
Feel free to clone this repo locally and use your own editor of choice.

If you would rather use the Gitpod development environment for this app:

- Change the dropdown that says "Web IDE" to "Gitpod" (if it already says "Gitpod" skip this step)
- Click the button that says "Gitpod"

https://drive.google.com/file/d/1APt-A5MRC4swWKfrvhuAAb7ALMpNUtHZ/view?usp=sharing

